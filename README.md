# Stranger GOP

## Tabella dei Contenuti

- [Panoramica](#panoramica)
- [Compilare](#compilare)
- [Eseguire](#eseguire)
- [Regole](#regole)
  - [Giocatori](#giocatori)
  - [Mappa](#mappa)
  - [Mazzo di Carte](#mazzo-di-carte)
  - [Dadi](#dadi)
- [Organizzazione del Progetto](#organizzazione-del-progetto)

## Panoramica

Stranger GOP è la rivisitazione del classico Gioco dell’Oca. Trasportati nel misterioso mondo di Stranger Things, i giocatori devono attraversare una serie di pericoli per scappare dalle grinfie del Mind Flayer. Tuttavia l’avversario più temibile sarà la mappa, che oltre ad essere generata ad ogni partita, nasconde varchi verso l’oscuro e misterioso Sottosopra.

## Compilare

Per compilare ed eseguire il gioco è necessario soddisfare alcune dipendenze:
  - **SDL2 2.0.8** https://www.libsdl.org/release/SDL2-2.0.8.zip
  - **SDL2_IMAGE** https://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.3.zip
  - **SDL2_TTF** https://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.14.zip
  - **cmake**
  
Per installarle da source sarà necessario ripetere la stessa procedura per tutte e tre le librerie:
  1) Decomprimere il .zip
  2) Creare una cartella "build" all'interno della cartella appena estratta
  3) Navigare all'interno di "build"
  4) Eseguire ../configure && make && sudo make install

Consigliamo l'installazione di queste dipendenze da source per la massima compatibilità.

Dopo aver installato le dipendenze sarà possibile compilare il gioco, la procedura varia in base al tipo di installazione
adottata per SDL2:
  - SDL2 Source: eseguire cmake -Dsdl2_source=ON . && make
  - SDL2 da repository: eseguir cmake . && make

## Eseguire

Per eseguire lanciare il file binario build/gop.run

## Regole

L’obiettivo del gioco consiste nell’attraversare la mappa per raggiungere l’ultima casella. Vince la partita il primo giocatore a raggiungere tale casella.

### Giocatori

L’ordine dei giocatori viene deciso in maniera arbitraria dal gioco. Ad ogni turno il giocatore deve lanciare i 3 dadi.

### Mappa

La lunghezza della mappa e gli effetti all’interno di ogni cella vengono generati casualmente ad ogni partita. Gli effetti possibili sono:
1. Pesca una carta;
2. Avanza di 2 caselle;
3. Lancia i dadi e torna indietro della somma dei loro valori;
4. Rilancia i dadi;
5. Scambia i mondi (entra nel sottosopra se sei nel mondo normale, e viceversa).


### Mazzo di Carte

Vi è un mazzo di carte che viene rimescolato ad ogni partita. Ogni carta presenta due effetti tra cui scegliere sul momento. Gli effetti possibili sono:
1. Avanza di 2 caselle;
2. Gli altri giocatori indietreggiano di un numero di caselle proporzionale alla mappa;
3. Il prossimo giocatore perde il turno;
4. Scambia la tua posizione col primo giocatore;
5. Indietreggia di un numero di caselle proporzionale alla grandezza della mappa;
6. Il prossimo giocatore avanza di un numero di caselle proporzionale alla grandezza della mappa;
7. Scambia la tua posizione con quella dell’ultimo giocatore;
8. Perdi il prossimo turno.

### Dadi

Vi sono tre dadi, due dei quali sono dadi classici, il terzo presenta i seguenti effetti:
* Mondo Superiore:
  1. Tre facce, ciascuna con semplici valori numerici (1, 2, 3) da sommare al valore complessivo degli altri due dadi;
  2. Una faccia che permette al giocatore di scegliere se andare avanti del singolo valore di uno dei due dadi ordinari, oppure del valore della loro somma;
  3. Una faccia che obbliga il giocatore a muoversi della somma dei due dadi ordinari per poi andare immediatamente alla prima casella contenente un effetto;
  4. Una faccia che obbliga il giocatore a muoversi della differenza (positiva) dei due dadi ordinari.
* Sottosopra:
  1. Tre facce, ciascuna con semplici valori numerici (1, 2, 3) da sottrarre al valore complessivo degli altri due dadi, nel caso di somma negativa si torna indietro;
  2. Una faccia che permette al giocatore di scegliere se andare avanti del singolo valore di uno dei due dadi ordinari, oppure del valore della loro somma;
  3. Una faccia che obbliga il giocatore a tornare indietro della somma dei due dadi ordinari;
  4. Una faccia che obbliga il giocatore a saltare il prossimo turno nel caso la somma degli altri dadi sia pari.


## Organizzazione del Progetto

L’applicazione è scritta in C++. Prevede l’utilizzo di 3 tool:
1. CMake per il controllo del build process dell’applicazione, che viene poi compilata tramite CLang;
2. GoogleTest per il test degli oggetti di gioco e di alcuni aspetti della logica;
3. SDL per la grafica del gioco.

Il progetto è stato suddiviso in 3 parti principali:
* Objects, contenente i modelli del gioco:
  * Card - definisce gli attributi e i metodi di una carta;
  * Cell - definisce gli attributi e i metodi di una casella della mappa;
  * Deck - definisce il modello del mazzo di carte, contenente un vector di tipo Card e i metodi di inizializzazione e sorteggio delle carte;
  * Map - definisce il modello della mappa, contenente un vector di tipo Cell e i metodi di creazione della mappa e di restituzione delle celle;
  * Effect - definisce la struttura di un effetto tramite un Enum (Action) e una mappatura delle probabilità di comparsa di ciascun tipo di effetto. Effect viene utilizzato come attributo all’interno di Card e Cell. L’assegnamento degli effetti a ciascuna carta o cella avviene all’interno dei metodi di inizializzazione di Deck e Map, in base alla distribuzione della loro probabilità;
  * Player - definisce il modello di ciascun giocatore, tramite degli attributi contenenti il nome, la posizione sulla mappa, la mappa attuale (sottosopra o mondo normale), lo Status e i metodi per modificare e restituire tali attributi;
  * Status - contiene la verifica della validità del turno attuale del giocatore, viene utilizzato per la scalabilità, nel caso dell’eventuale aggiunta di elementi al gioco nel futuro.
* Game Logic, contenente la logica del gioco e lo stato globale:
  * Gamelogic - contenente i modelli principali per la logica del gioco:
  * DiceRollHandler - si occupa della gestione del lancio dei dadi e della logica del dado speciale e dell’applicazione dei suoi effetti;
  * EffectHandler - si occupa di eseguire gli effetti delle carte o delle caselle in maniera corretta;
  * GameLogic - contiene istanze di GameStatus (lo stato globale del gioco), DiceRollHandler, EffectHandler e GameView (la parte grafica dell’applicazione). Si occupa principalmente dell’esecuzione corretta del ciclo di gioco definito nel metodo start, con l’ausilio dei metodi checkCellEffect e statusCheck.
  * Gamestatus - definisce lo stato globale della partita e contiene un’istanza del vector contenente i giocatori, le due istanze delle mappe e l’istanza del deck, in sintesi tutti gli oggetti del gioco che valgono nella partita attuale. I metodi della classe GameStatus si occupano principalmente della restituzione e modifica di questi oggetti, nonché dell’incremento e verifica dei turni di gioco.
* Graphics, la parte che si occupa della gestione della grafica del gioco e dell’interazione con l’utente. Tramite l’utilizzo della libreria SDL, sono stati definiti i seguenti oggetti visuali:
  * Background - lo sfondo del gioco;
  * Button - il pulsante che ci permette di lanciare i dadi;
  * Dice - la rappresentazione grafica dei dadi;
  * Tile - la rappresentazione grafica delle caselle;
  * Messagebox - il componente grafico dove vengono visualizzati messaggi utili per il giocatore;
  * Texturemanager - il gestore delle texture;
  * Playerview - il gestore della visuale del giocatore;
  * Gameview - la classe che racchiude tutti i principali elementi della grafica. Questa classe viene utilizzata e gestita all’interno della GameLogic;

