#include "logger.hpp"
#include <string.h>
#include <iostream>
using namespace std;
using namespace gopgl;

static bool DEBUG = true;

void gopgl::log_message(string &message)
{
    if (DEBUG)
    {
        cout << message << endl;
    }
}

void gopgl::log_message(int message)
{
    if (DEBUG)
    {
        cout << message << endl;
    }
}

void gopgl::log_message(const char *message)
{
    if (DEBUG)
    {
        cout << message << endl;
    }
}
