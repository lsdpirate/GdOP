#ifndef _H_GLEXCEPTIONS_
#define _H_GLEXCEPTIONS_

#include <stdexcept>
#include <string>

namespace gopgl{
	class INVALID_DIE_FACES_EXCEPTION : public std::range_error{
		public:
			INVALID_DIE_FACES_EXCEPTION(const std::string & arg) : range_error(arg){}
	};
}

#endif