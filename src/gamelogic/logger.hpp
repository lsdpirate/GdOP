#ifndef _LOGGER_H_
#define _LOGGER_H_
#include <string>

namespace gopgl{
void log_message(std::string& message);
void log_message(int message);
void log_message(const char* message);
	
}

#endif