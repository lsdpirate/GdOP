#include "gamelogic.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string.h>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "../objects/card.hpp"
#include "../objects/cell.hpp"
#include "../objects/deck.hpp"
#include "../objects/effect.hpp"
#include "../objects/map.hpp"
#include "../objects/player.hpp"
#include "gamestatus.hpp"
#include "logger.hpp"
using namespace std;
using namespace gopgl;
using namespace gopo;
using namespace graphics;

GameLogic::GameLogic(int playerCount, string *playerNames, int mapLength) {
    this->gameStatus = new GameStatus(playerCount, playerNames, mapLength);
    this->diceRollHandler = new DiceRollHandler(this->gameStatus);
    this->effectHandler =
        new EffectHandler(this->gameStatus, this->diceRollHandler);
    this->gameview = new GameView();
}

GameLogic::GameLogic(GameOptions gameOptions) {
    std::string *playerNames = new string[gameOptions.getNumberOfPlayers()];
    for (int i = 0; i < gameOptions.getNumberOfPlayers(); i++) {
        playerNames[i] = gameOptions.getPlayerNames().at(i);
    }
    this->gameStatus = new GameStatus(gameOptions.getNumberOfPlayers(),
                                      playerNames, gameOptions.getMapLength());
    this->diceRollHandler = new DiceRollHandler(this->gameStatus);
    this->effectHandler =
        new EffectHandler(this->gameStatus, this->diceRollHandler);
    this->gameview = new GameView();
}

GameLogic::~GameLogic() {
    delete this->effectHandler;
    delete this->diceRollHandler;
    delete this->gameStatus;
    delete this->gameview;
}

void GameLogic::start() {
    bool gameOver = false;
    gameview->init(this->gameStatus);
    int r = 0;
    while (!gameOver && !gameview->isClosed()) {
        this->gameStatus->newTurn();
        Player *currentPlayer;
        while ((currentPlayer = this->gameStatus->getCurrentPlayer())->losesNextTurn()) {
            gameview->printMessage("You lose this turn!", "");
            gameview->messageDialog("Skip");
            gameview->printMessage("", "");
            currentPlayer->setLoseNextTurn(false);
            this->gameStatus->newTurn();
        }
        gameOver = gameview->renderNewTurnStage(currentPlayer->getName()) == -1;

        if (!gameOver) {
            gameview->setRollStage();
            gameOver = gameview->messageDialog("Roll") == -1;
        }

        if (!gameview->isClosed() && !gameOver) {
            gameOver = this->runRollPhase();
            int playersCellNumber = currentPlayer->getPosition();

            if (!gameOver) {
                Cell *playersCell = NULL;
                do {
                    /*  Effect chain loop
                        Continue activating effects on the player
                        as long as he/she lands on a non-empty cell.
                    */
                    playersCellNumber = currentPlayer->getPosition();
                    if(playersCellNumber >= this->gameStatus->getUpperMap()->getSize()){
                        playersCellNumber = this->gameStatus->getUpperMap()->getSize() - 1;
                    }
                    if (currentPlayer->isInLowerMap()) {
                        playersCell =
                            this->gameStatus->getLowerMap()->getCell(playersCellNumber);
                    } else {
                        playersCell =
                            this->gameStatus->getUpperMap()->getCell(playersCellNumber);
                    }
                    gameview->printMessage("Cell effect",
                                           playersCell->getEffect()->getDescription());
                    if (playersCell->getEffect()->getAction() == Action::drawOne) {
                        gameOver = this->runCardDrawPhase();
                    } else {
                        gameview->setCameraX((this->gameStatus->getCurrentPlayer()->getPosition() - 6) * 150);
                        gameOver = gameview->messageDialog("Continue") == -1;
                        this->checkCellEffect(playersCell);
                    }

                    // Quick and dirty way to visualize rethrow's dice
                    // if a rethrow happened.
                    // Uses DiceHandler::roll()'s side effect
                    // of memorizing the last throw.
                    if (playersCell->getEffect()->getAction() == Action::rethrow ||
                        playersCell->getEffect()->getAction() == Action::throwAndGoBack) {
                        int *dice = this->diceRollHandler->getLastRoll();
                        gameview->setDie(0, dice[0]);
                        gameview->setDie(1, dice[1]);
                    }

                    // Win condition check.
                    if (currentPlayer->getPosition() >=
                        this->gameStatus->getCellCount() - 1) {
                        log_message("A player won!");
                        currentPlayer->setPosition(this->gameStatus->getCellCount());
                        this->gameview->setVictory();
                        this->gameview->printMessage("You won!", "Close the game to continue");
                        this->gameview->messageDialog("Close");
                        gameOver = true;
                    }
                } while (playersCell->getEffect()->getAction() != Action::empty &&
                         playersCellNumber != currentPlayer->getPosition() &&
                         !gameOver);
            }
        }
    }
    gameview->clean();
}

void GameLogic::checkCellEffect(Cell *playersCell) {
    Effect *effect = playersCell->getEffect();
    this->effectHandler->handleEffect(*effect);
}

void GameLogic::statusCheck(Player &player) { return; }

void EffectHandler::handleEffect(const Effect &e) {
    switch (e.getAction()) {
        case drawOne: {
            this->handleDraw();
            break;
        }
        case teleport: {
            this->handleGoForward();
            break;
        }

        case throwAndGoBack: {
            this->handleThrowBack();
            break;
        }
        case rethrow: {
            this->handleRethrow();
            break;
        }
        case switchWorld: {
            this->handleSwitchWorld();
            break;
        }
        case goForward: {
            this->handleGoForward();
            break;
        }
        case enemiesBack: {
            this->handleEnemiesBack();
            break;
        }
        case enemyLosesTurn: {
            this->handleEnemyLosesTurn();
            break;
        }
        case switchFirst: {
            this->handleSwitchFirst();
            break;
        }
        case goBack: {
            this->handleGoBack();
            break;
        }
        case enemyForward: {
            this->handleEnemyForward();
            break;
        }
        case switchLast: {
            this->handleSwitchLast();
            break;
        }
        case loseTurn: {
            this->handleLoseTurn();
            break;
        }
        default: { break; }
    }
}

void EffectHandler::handleDraw() {
    Card card = this->gameStatus.getDeck()->draw();
    // TODO: draw handling should be moved to the game loop method.
    this->handleEffect(card.getEffect1());
}

void EffectHandler::jump(Player *player, int cellCount, bool back) {
    int toJump = 0;
    log_message("Jumping");
    if (back) {
        if (cellCount > 120) {
            toJump = -4;
        } else if (cellCount > 80) {
            toJump = -3;
        } else {
            toJump = -2;
        }
    } else {
        if (cellCount > 120) {
            toJump = 4;
        } else if (cellCount > 80) {
            toJump = 3;
        } else {
            toJump = 2;
        }
    }

    player->move(toJump);
    log_message("Jumped");
}

void EffectHandler::handleThrowBack() {
    int *dice = this->diceRollHandler->roll(true);
    int steps = dice[0] + dice[1];
    Player *player = this->gameStatus.getCurrentPlayer();
    player->move(-steps);
}

void EffectHandler::handleRethrow() {
    int *dice = this->diceRollHandler->roll(true);
    int steps = dice[0] + dice[1];
    Player *player = this->gameStatus.getCurrentPlayer();
    player->move(steps);
}

void EffectHandler::handleSwitchWorld() {
    Player *player = this->gameStatus.getCurrentPlayer();
    if (player->isInLowerMap()) {
        player->setInLowerMap(false);
    } else {
        player->setInLowerMap(true);
    }
}

void EffectHandler::handleGoForward() {
    Player *player = this->gameStatus.getCurrentPlayer();
    player->move(2);
}

void EffectHandler::handleEnemiesBack() {
    int cellCount = this->gameStatus.getCellCount();
    std::vector<Player> *players = this->gameStatus.getPlayers();

    for (int i = 0; i < this->gameStatus.getPlayerCount(); i++) {
        if (i != this->gameStatus.getCurrentPlayerIndex()) {
            this->jump(&players->at(i), cellCount, true);
        }
    }
}

void EffectHandler::handleEnemyLosesTurn() {
    Player *next = this->gameStatus.getNextPlayer();
    next->setLoseNextTurn(true);
}

void EffectHandler::handleSwitchFirst() {
    Player *currPlayer = this->gameStatus.getCurrentPlayer();
    Player *firstPlayer = this->gameStatus.getFirstPlayer();
    int currPosition = currPlayer->getPosition();

    currPlayer->setPosition(firstPlayer->getPosition());
    firstPlayer->setPosition(currPosition);
}

void EffectHandler::handleGoBack() {
    int cellCount = this->gameStatus.getCellCount();
    Player *player = this->gameStatus.getCurrentPlayer();
    this->jump(player, cellCount, true);
}

void EffectHandler::handleEnemyForward() {
    Player *nextPlayer = this->gameStatus.getNextPlayer();
    int cellCount = this->gameStatus.getCellCount();
    this->jump(nextPlayer, cellCount);
}

void EffectHandler::handleSwitchLast() {
    Player *currPlayer = this->gameStatus.getCurrentPlayer();
    Player *lastPlayer = this->gameStatus.getLastPlayer();
    int currPosition = currPlayer->getPosition();

    currPlayer->setPosition(lastPlayer->getPosition());
    lastPlayer->setPosition(currPosition);
}

void EffectHandler::handleLoseTurn() {
    Player *player = this->gameStatus.getCurrentPlayer();
    player->setLoseNextTurn(true);
}

int *DiceRollHandler::roll(bool simple_roll) {
    int d1Roll = rand() % 6 + 1;
    int d2Roll = rand() % 6 + 1;

    int effectDieRoll = rand() % 6 + 1;
    int *result = new int[3];
    result[0] = d1Roll;
    result[1] = d2Roll;
    result[2] = effectDieRoll;

    Player *currentPlayer = this->gameStatus.getCurrentPlayer();
    if (!simple_roll) {
        if (effectDieRoll == 4) {
            // Because of UI needs, this case is handled externally.
        } else if (effectDieRoll == 5) {
            if (currentPlayer->isInLowerMap()) {
                this->handleMoveBackwards(d1Roll + d2Roll);
            } else {
                this->handleMoveToEffect(d1Roll + d2Roll);
            }
        } else if (effectDieRoll == 6) {
            if (currentPlayer->isInLowerMap()) {
                this->handleEvenIsLostTurn(d1Roll + d2Roll);
            } else {
                this->handleSubtractRoll(d1Roll, d2Roll);
            }
        } else {
            int finalRoll;
            if (!currentPlayer->isInLowerMap()) {
                finalRoll = effectDieRoll + d1Roll + d2Roll;
            } else {
                finalRoll = -effectDieRoll + d1Roll + d2Roll;
            }
            currentPlayer->move(finalRoll);
        }
    }
    this->lastRoll = result;
    return result;
}

int *DiceRollHandler::getLastRoll() { return this->lastRoll; }

int DiceRollHandler::handleMoveToEffect(int roll) {
    Player *p = this->gameStatus.getCurrentPlayer();
    int pos = p->getPosition();
    int targetPos = pos + roll;
    Map *map = !p->isInLowerMap() ? this->gameStatus.getUpperMap()
                                  : this->gameStatus.getLowerMap();
    // if the player hasn't won apply the effect.
    if (targetPos < map->getSize() - 1) {
        Cell *cell = map->getCell(targetPos);
        // start searching for the next cell with an effect
        // starting from the one the player would land on.
        // If the cell the player lands on has an effect then
        // the player will stay there.
        int i = 0;
        while (cell != NULL && cell->getEffect()->getAction() == Action::empty) {
            i++;
            cell = this->gameStatus.getUpperMap()->getCell(targetPos + i);
        }

        if (cell != NULL) {
            targetPos += i;
        }
    }
    p->setPosition(targetPos);
    return targetPos;
}

int DiceRollHandler::handleSubtractRoll(int r1, int r2) {
    int roll = r1;
    if (r1 > r2) {
        roll -= r2;
    } else {
        roll = r2 - r1;
    }
    Player *p = this->gameStatus.getCurrentPlayer();
    return p->move(roll);
}

int DiceRollHandler::handleMoveBackwards(int roll) {
    Player *p = this->gameStatus.getCurrentPlayer();
    return p->move(-roll);
}

int DiceRollHandler::handleEvenIsLostTurn(int roll) {
    int result = 0;
    if (roll % 2 != 0) {
        result = this->gameStatus.getCurrentPlayer()->move(roll);
    } else {
        result = this->gameStatus.getCurrentPlayer()->getPosition();
    }
    return result;
}

int GameLogic::handleDiceChoice(int *dice) {
    int result = 0;
    int ans = this->gameview->specialDiceChoiceDialog();
    if (ans == 0) {
        result = dice[0];
    } else if (ans == 1) {
        result = dice[1];
    } else if (ans == 2) {
        result = dice[1] + dice[0];
    } else {
        result = -1;
    }
    return result;
}

bool GameLogic::runRollPhase() {
    int *dice = this->diceRollHandler->roll();
    bool gameOver = false;

    for (int i = 0; i < 3; i++) {
        this->gameview->setDie(i, dice[i]);
    }

    // TODO give a name to effect dice constants
    if (dice[2] == 4) {
        int r = this->handleDiceChoice(dice);
        gameOver = r == -1;
        this->gameStatus->getCurrentPlayer()->move(r);
    }

    return gameOver;
}

bool GameLogic::runCardDrawPhase() {
    gameview->setCameraX((this->gameStatus->getCurrentPlayer()->getPosition() - 6) * 150);
    bool gameOver = gameview->messageDialog("Draw...") == -1;
    int r;
    Card card = this->gameStatus->getDeck()->draw();
    if (!gameOver) {
        r = gameview->cardEffectPickingDialog(&card);
        gameOver = r == -1;
        if (r == 0) {
            this->effectHandler->handleEffect(card.getEffect1());
        } else if (r == 1) {
            this->effectHandler->handleEffect(card.getEffect2());
        }
    }
    return gameOver;
}
