#ifndef _H_EFFECT_
#define _H_EFFECT_

#include <map>
#include <string>
#include "../gamelogic/gamestatus.hpp"

namespace gopo {
enum Action {
    empty,
    drawOne,
    teleport,
    throwAndGoBack,
    rethrow,
    switchWorld,
    END_OF_CELL_EFFECTS,
    goForward,
    enemiesBack,
    enemyLosesTurn,
    switchFirst,
    goBack,
    enemyForward,
    switchLast,
    loseTurn,
    LAST_ACTION
};

const std::map<Action, int> EFFECTS_PROBABILITY_DISTR = {
    {empty, 60},        {drawOne, 15},        {goBack, 11},
    {goForward, 11},    {enemyLosesTurn, 11}, {rethrow, 10},
    {loseTurn, 11},     {throwAndGoBack, 10}, {switchFirst, 11},
    {enemyForward, 11}, {switchLast, 11},     {enemiesBack, 11},
    {teleport, 5},      {switchWorld, 11}
};

class Effect {
   private:
    Action action;
    int number;
    std::string name;
    std::string description;
    std::string seriousDescription;

   public:
    Effect();
    Effect(Action);
    const Action getAction() const;
    ~Effect();
    std::string getName();
    std::string getDescription();
    int getNumber();
};
}

#endif
