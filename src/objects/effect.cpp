#include "effect.hpp"

using namespace gopo;

Effect::Effect() {
    this->number = 13;
    this->name = "Empty";
    this->description = "Nothing interesting, just wait for your next turn.";
    this->seriousDescription = "";
    this->action = empty;
}

Effect::Effect(Action action) {
    this->action = action;

    switch (this->action) {
        case drawOne:
            this->number = 0;
            this->name = "Draw";
            this->description = "Draw a card! Will it help or will it hurt?";
            this->seriousDescription = "Draw a Card";
            break;
        case teleport:
            this->number = 1;
            this->name = "Teleport";
            this->description = "Eleven channels her telekinesis on you. Go forward!";
            this->seriousDescription = "Go Ahead 2 Cells";
            break;
        case throwAndGoBack:
            this->number = 2;
            this->name = "Run Back!";
            this->description = "The Demogorgon stands on your path! Roll the dice and run back!";
            this->seriousDescription = "Roll the Dice and Go Back by their sum";
            break;
        case rethrow:
            this->number = 3;
            this->name = "Re-throw";
            this->description = "The path looks secure! Roll the dice and go forward.";
            this->seriousDescription = "Roll the Dice again";
            break;
        case switchWorld:
            this->number = 4;
            this->name = "Switch World";
            this->description = "Well well! Guess who found a portal...";
            this->seriousDescription = "Switch your current World";
            break;
        case goForward:
            this->number = 5;
            this->name = "Rush";
            this->description = "Something strange is approaching. Rush forward!";
            this->seriousDescription = "Go Ahead 2 Cells";
            break;
        case enemiesBack:
            this->number = 6;
            this->name = "Enemies Go Back";
            this->description = "Roots are spawning under your enemies, bringing them further from victory.";
            this->seriousDescription = "Enemies Go Back by some Cells";
            break;
        case enemyLosesTurn:
            this->number = 7;
            this->name = "Frightened Foe";
            this->description = "You scared your enemies. The next player shall lose his turn!";
            this->seriousDescription = "The Next Player loses his Turn";
            break;
        case switchFirst:
            this->number = 8;
            this->name = "Switch First Place";
            this->description = "You're the first now! Take a look back at your friends.";
            this->seriousDescription = "Switch Places with the first Player";
            break;
        case goBack:
            this->number = 9;
            this->name = "Rush Back";
            this->description = "A Demodog is approaching! Rush back and find a safe place.";
            this->seriousDescription = "Go Back by some Cells";
            break;
        case enemyForward:
            this->number = 10;
            this->name = "Enemy Goes Forward";
            this->description = "You stopped for some Eggos...";
            this->seriousDescription = "The Next Player Goes Ahead by some Cells";
            break;
        case switchLast:
            this->number = 11;
            this->name = "Switch Last Place";
            this->description = "You always want to help a foe in need.";
            this->seriousDescription = "Switch Places with the Last Player";
            break;
        case loseTurn:
            this->number = 12;
            this->name = "Lose Turn";
            this->description = "Hopper is really angry with you. You're grounded! For one turn...";
            this->seriousDescription = "Lose your next Turn";
            break;
        default:
            this->number = 13;
            this->name = "Empty";
            this->description = "Nothing interesting, just wait for your next turn.";
            this->seriousDescription = "";
            break;
    }
}

const Action Effect::getAction() const{
    return this->action;
}

std::string Effect::getName(){
  return this->name;
}
std::string Effect::getDescription(){
  return this->seriousDescription;
}
int Effect::getNumber(){
  return this->number;
}

Effect::~Effect() {}
