// #include <string>
// #include "./status.hpp"
#include "./player.hpp"

using namespace std;
using namespace gopo;

Player::Player() {
    this->name = "";
    this->position = 0;
    this->inLowerMap = false;
    this->loseNextTurn = false;
}

Player::Player(string &name) {
    this->name = name.substr(0);
    this->position = 0;
    this->loseNextTurn = false;
    this->inLowerMap = false;
}

Player::Player(string name) {
    this->name = name;
    this->position = 0;
    this->inLowerMap = false;
    this->loseNextTurn = false;
}

// increments player's position by a certain amount an returns the new position
int Player::move(int steps) {
    this->position = this->position + steps >= 0 ? this->position + steps : 0;
    return this->getPosition();
}

// returns player's position
int Player::getPosition() { return this->position; }

bool Player::isInLowerMap() {
    return this->inLowerMap;
}

void Player::setName(const string &name) { this->name = string(name); }


void Player::setPosition(int position) { this->position = position; }

Player::~Player() {
}

string Player::getName() {
    string result = string(this->name);
    return result;
}

void Player::setInLowerMap(bool inLowerMap) {
    this->inLowerMap = inLowerMap;
}

bool Player::setLoseNextTurn(bool b){
    this->loseNextTurn = b;
}
bool Player::losesNextTurn(){
    return this->loseNextTurn;
}