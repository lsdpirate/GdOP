#ifndef _H_MAP_
#define _H_MAP_

#include <vector>
#include "./cell.hpp"

namespace gopo {
	class Map {
	private:
		std::vector<Cell> cells;
		int size;

	public:
		Map(int);
		int getSize();
		gopo::Cell *getCell(int);
		const std::vector<gopo::Cell> &getCells() const;
		static Map createMap(int);
		void assignEffects();
		~Map();
	};
}

#endif
