#include "./card.hpp"
#include "effect.hpp"

using namespace gopo;
using namespace std;

Card::Card(Effect effect1, Effect effect2) {
  this->setEffect1(effect1);
  this->setEffect2(effect2);
}

void Card::setEffect1(Effect effect) {
  this->effect1 = effect;
}

void Card::setEffect2(Effect effect) {
  this->effect2 = effect;
}

Effect Card::getEffect1() {
  return this->effect1;
}

Effect Card::getEffect2() {
  return this->effect2;
}


Card::~Card() {

}
