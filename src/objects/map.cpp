#include "./map.hpp"
#include <stdexcept>
#include <vector>
#include "./cell.hpp"
#include <cstdlib>
#include "ctime"
using namespace gopo;
using namespace std;

Map::Map(int cellsNum) : cells(0, Cell(Effect(Action::empty))) {
    this->size = cellsNum;
    this->assignEffects();
}

Map Map::createMap(int cellsNum) { return Map(cellsNum); }

void Map::assignEffects() {
    srand(time(0));
    int probCeil = 0;
    int effectIndex = Action::empty;
    while (effectIndex < Action::END_OF_CELL_EFFECTS) {
        probCeil += EFFECTS_PROBABILITY_DISTR.at((Action)effectIndex);
        effectIndex++;
    }

    this->cells.push_back(Cell());
    for (int i = 1; i < this->size; i++) {
        effectIndex = -1;
        int randInt = (rand() % probCeil) + 1;
        do{
            effectIndex++;
            randInt -= EFFECTS_PROBABILITY_DISTR.at((Action)effectIndex);
        }while(effectIndex < Action::END_OF_CELL_EFFECTS && randInt > 0);

        Cell newCell = Cell(Effect((Action)effectIndex));
        this->cells.push_back(newCell);
    }
}

Cell* Map::getCell(int position) {
    Cell* res;
    try {
        res = &this->cells.at(position);
    } catch (out_of_range e) {
        res = NULL;
    }
    return res;
}

const std::vector<gopo::Cell>& Map::getCells() const { return this->cells; }

int Map::getSize(){
	return this->cells.size();
}
Map::~Map() {}
