#include "playerview.hpp"
#include "gameview.hpp"
#include "texturemanager.hpp"
#include "../objects/player.hpp"
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <math.h>

using namespace graphics;

PlayerView::PlayerView(const char* bg,const char* minimapPlayer,int playerN,int totPlayers, gopo::Player* player, int mapLength){
  mapL = mapLength;
  p = player;
  playerCount = totPlayers;
  playerNumber = playerN;
  objTexture = TextureManager::loadTexture(bg);
  minimapTexture = TextureManager::loadTexture(minimapPlayer);
  char const* playerchar;
  int playerNameLength = player->getName().length();
  if (playerNameLength > 9) {
    playerchar = player->getName().substr(0,7).append("...").c_str();
  }else{
    playerchar = player->getName().c_str();
  }
  Rmod = (playerN & 1) * 255;
  Gmod = (playerN & 2) * 255;
  Bmod = (playerN & 4) * 255;
  std::cout << playerN << ": (" << Rmod << "," << Gmod << "," << Bmod << ")" <<'\n';
  if ((Rmod + Gmod + Bmod) / 3 < 128) {
    playerNameTexture = TextureManager::createTextureFromText(playerchar,"./assets/OpenSans-ExtraBold.ttf",14,{255,255,255,255});
  }else{
    playerNameTexture = TextureManager::createTextureFromText(playerchar,"./assets/OpenSans-ExtraBold.ttf",14,{0,0,0,255});
  }
}


void PlayerView::update(SDL_Rect camera,gopo::Player* currentPlayer ,Uint32 tick) {
  int scalew = camera.w / 1280;
  int scaleh = camera.h / 720;
  srcRectObj.x = 0;
  srcRectObj.y = 0;
  srcRectObj.h = 90;
  srcRectObj.w = 90;
  destRectObj.x = (p->getPosition() * 150) + 30 + (200/(playerCount+1) * (playerNumber+1)) * scalew - camera.x;
  if (currentPlayer == p) {
    destRectObj.y = 15 * sin(tick/100) + 385 - (90 / (playerCount+1) * (playerNumber+1)) * scaleh - camera.y;
  } else {
    destRectObj.y = 400 - (90 / (playerCount+1) * (playerNumber+1)) * scaleh - camera.y;
  }
  destRectObj.w = srcRectObj.w * scalew;
  destRectObj.h = srcRectObj.h * scaleh;

  srcRectMP = {0,0,100,150};
  int L = (500 / mapL) ;
  destRectMP.x = 765 + L*p->getPosition();
  destRectMP.y = 23 - playerNumber;
  destRectMP.w = 20;
  destRectMP.h = 30;

  SDL_Rect playerNameRect;
  SDL_QueryTexture(playerNameTexture, NULL, NULL, &playerNameRect.w, &playerNameRect.h);
  srcRectPlayerName.x = 0;
  srcRectPlayerName.y = 0;
  srcRectPlayerName.h = playerNameRect.h;
  srcRectPlayerName.w = playerNameRect.w;
  destRectPlayerName.x = destRectObj.x + ((destRectObj.w - playerNameRect.w)/2) * scalew;
  destRectPlayerName.y = destRectObj.y + ((destRectObj.h - playerNameRect.h)/2) * scalew;
  destRectPlayerName.h = playerNameRect.h * scalew;
  destRectPlayerName.w = playerNameRect.w * scaleh;

}

void PlayerView::render(bool worldFocus){
  if (worldFocus != p->isInLowerMap()) {
    SDL_SetTextureAlphaMod(this->objTexture, 240);
  } else {
    SDL_SetTextureAlphaMod(this->objTexture, 30);
  }
  SDL_SetTextureAlphaMod(this->minimapTexture, 200);
  SDL_SetTextureColorMod(this->objTexture, Rmod,Gmod,Bmod);
  SDL_SetTextureColorMod(this->minimapTexture, Rmod,Gmod,Bmod);
  TextureManager::draw(this->objTexture, srcRectObj, destRectObj);
  TextureManager::draw(this->minimapTexture, srcRectMP, destRectMP);
  TextureManager::draw(this->playerNameTexture, srcRectPlayerName , destRectPlayerName);
}

PlayerView::~PlayerView(){
  SDL_DestroyTexture(objTexture);
  SDL_DestroyTexture(minimapTexture);
  SDL_DestroyTexture(playerNameTexture);
}
