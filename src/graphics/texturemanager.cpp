#include "./texturemanager.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "gameview.hpp"

using namespace graphics;
using namespace gopgl;

SDL_Texture* TextureManager::loadTexture(char const* filename){
  SDL_Surface* tempSurface = IMG_Load(filename);
  SDL_Texture* tex = SDL_CreateTextureFromSurface(GameView::renderer, tempSurface);
  SDL_FreeSurface(tempSurface);
  return tex;
}
SDL_Texture* TextureManager::createTextureFromText(const char* text, const char* font, int size, SDL_Color color){
  TTF_Font* tempFont = TTF_OpenFont(font, size);
  if(!tempFont){
    printf("TTF_OpenFont: %s\n", TTF_GetError());
  }
  SDL_Surface* tempSurface = TTF_RenderText_Solid(tempFont, text, color);
  SDL_Texture* tex = SDL_CreateTextureFromSurface(GameView::renderer, tempSurface);
  SDL_FreeSurface(tempSurface);
  return tex;
}
SDL_Texture* TextureManager::createTextureFromTextBold(const char* text, SDL_Color color){
  SDL_Surface* tempSurface = TTF_RenderText_Solid(GameView::globalFont, text, color);
  SDL_Texture* tex = SDL_CreateTextureFromSurface(GameView::renderer, tempSurface);
  SDL_FreeSurface(tempSurface);
  return tex;
}
SDL_Texture* TextureManager::createTextureFromTextLight(const char* text, SDL_Color color){
  SDL_Surface* tempSurface = TTF_RenderText_Solid(GameView::messageboxFont, text, color);
  SDL_Texture* tex = SDL_CreateTextureFromSurface(GameView::renderer, tempSurface);
  SDL_FreeSurface(tempSurface);
  return tex;
}
void TextureManager::draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest){
  SDL_RenderCopy(GameView::renderer, tex, &src, &dest);
}
