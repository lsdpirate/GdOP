#ifndef _H_GAMEVIEW_
#define _H_GAMEVIEW_
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "../gamelogic/gamestatus.hpp"
#include "../objects/card.hpp"
#include "texturemanager.hpp"
#include "tile.hpp"
#include "background.hpp"
#include "dice.hpp"
#include "./card.hpp"
#include "button.hpp"
#include "playerview.hpp"
#include "messagebox.hpp"
#include <string.h>
#include <iostream>
using namespace std;

namespace graphics{
  class GameView{
  public:
    GameView();
    void init(gopgl::GameStatus*);
    void loadAssets();
    void cameraMove();
    void update();
    void render();
    void clean();
    bool isClosed();
    bool getRoll();
    void setStage(int);
    void setDie(int, int);
    int getDie(int);
    int getStage();
    void setRoll(bool);
    void swipeCamera(int dest);
    void capFramerate(Uint32 startingTick);
    void setVictory();

    int cardEffectPickingDialog(gopo::Card* card);
    int messageDialog(string message);
    int specialDiceChoiceDialog();
    void printMessage(string m1, string m2);
    void setRollStage();
    void setCardStage();
    void unsetCardStage();
    int renderNewTurnStage(string playerName);
    void handleUIResponsiveness(SDL_Event event);
    void setCameraX(int x);
    ~GameView();

    static TTF_Font *globalFont;
    static TTF_Font *messageboxFont;
    static SDL_Renderer *renderer;
  private:
    SDL_Window* window;
    bool victory = false;
    bool isRunning;
    bool upperWorldFocus = true;
    bool roll = false;
    bool swipe = true;
    bool rollStage = true;
    bool diceChoiceStage = false;
    bool cardChoiceStage = false;
    bool moveStage = false;
    bool minimapUpdate = false;
    bool printingMessage = false;
    bool cardStage = false;
    int stage;

    bool flagDescription;
    bool minimapDescription;

    SDL_Rect camera;
    bool cameraUpdate;
    int cameraSpeed;
    int mouseDownX;
    int mouseDownY;
    int mouseX;
    int mouseY;
    Uint32 lastCameraUpdate;

    int mapLength;
    Tile* upperTiles[400];
    Tile* lowerTiles[400];
    Tile* upperEndTile;
    Tile* lowerEndTile;
    gopgl::GameStatus* gameStatus;

    Dice* dice1;
    Dice* dice2;
    Dice* diceLower;
    Dice* diceUpper;

    Background* upperBackground;
    Background* lowerBackground;
    Button* rollButton;
    Button* lowerWorldButton;
    Button* upperWorldButton;
    Button* centerButton;
    Button* topButton;
    Button* middleButton;
    Button* bottomButton;
    Button* leftButton;
    Button* rightButton;
    Button* minimapButton;
    Button* victoryScreen;
    MessageBox* messagebox;
    MessageBox* tempmessagebox;

    graphics::GCard* pickingCard;
    PlayerView* players[8];

    Uint32 frameStart;
    const int SCREEN_FPS = 60;
    const int SCREEN_TICK_PER_FRAME = 1000 /SCREEN_FPS;

    std::string diceEffectDescr[12] = {
      "Move by d1 + d2 + 1",
      "Move by d1 + d2 + 2",
      "Move by d1 + d2 + 3",
      "Choose to either use the sum of your dice or just one of them",
      "After moving by d1 + d2 jump to the first cell containing an effect",
      "Move by the highest of your dice minus the lowest",
      "Move by d1 + d2 - 1",
      "Move by d1 + d2 - 2",
      "Move by d1 + d2 - 3",
      "Choose to either use the sum of your dice or just one of them",
      "You move backwards by the sum of your two dice",
      "If the sum of your two dice is even, you lose the turn"

    };

    std::string specialDiceChoiceText[3] = {
      "D1",
      "D2",
      "D1 + D2"
    };
  };
}

#endif
