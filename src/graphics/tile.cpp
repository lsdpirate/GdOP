#include "tile.hpp"
#include "gameview.hpp"
#include "texturemanager.hpp"
#include "../objects/effect.hpp"
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

using namespace graphics;

Tile::Tile(const char* bg,const char* flags, int tn, gopo::Effect *effect){
  objTexture = TextureManager::loadTexture(bg);
  flagsTexture = TextureManager::loadTexture(flags);
  std::string s = std::to_string(tn + 1);
  char const* numchar = s.c_str();
  numberTexture = TextureManager::createTextureFromText(numchar,"./assets/OpenSans-ExtraBold.ttf",32,{255,255,255,255});
  this->eff = effect;
  tileNumber = tn;

}

Tile::Tile(const char* bg,SDL_Rect src, SDL_Rect dst){
  objTexture = TextureManager::loadTexture(bg);
  this->srcRectObj = src;
  this->destRectObj = dst;
}

SDL_Texture* Tile::getTexture(){ return objTexture; }
  std::string Tile::toString(){
  std::string str = "this is a tile";
  return str;
}
void Tile::update(SDL_Rect camera) {
  int scalew = camera.w / 1280;
  int scaleh = camera.h / 720;
  srcRectObj.x = 0;
  srcRectObj.y = 0;
  srcRectObj.h = 200;
  srcRectObj.w = 150;
  destRectObj.x = tileNumber * srcRectObj.w * scalew - camera.x;
  destRectObj.y = 400 * scaleh - camera.y;
  destRectObj.w = srcRectObj.w * scalew;
  destRectObj.h = srcRectObj.h * scaleh;
  SDL_Rect nRect;
  SDL_QueryTexture(numberTexture, NULL, NULL, &nRect.w, &nRect.h);
  srcRectNum.x = 0;
  srcRectNum.y = 0;
  srcRectNum.h = nRect.h;
  srcRectNum.w = nRect.w;
  destRectNum.x = destRectObj.x + ((destRectObj.w - nRect.w)/2) * scalew;
  destRectNum.y = (destRectObj.y + 90) + ((110 - nRect.h)/2) * scaleh;
  destRectNum.h = nRect.h * scalew;
  destRectNum.w = nRect.w * scaleh;
  srcRectFlag.x = 100 * eff->getNumber();
  srcRectFlag.y = 0;
  srcRectFlag.w = 90;
  srcRectFlag.h = 150;
  destRectFlag.x = destRectObj.x + 95;
  destRectFlag.y = destRectObj.y + 100;
  destRectFlag.h = 90;
  destRectFlag.w = 55;
}

void Tile::updateLastTile(SDL_Rect camera){
  srcRectObj = {0,0,350,200};
  drawDestRectObj = destRectObj;
  drawDestRectObj.x = destRectObj.x - camera.x;
}

void Tile::renderLastTile(){
  TextureManager::draw(this->objTexture, srcRectObj, drawDestRectObj);
}

void Tile::render(){
  TextureManager::draw(this->objTexture, srcRectObj, destRectObj);
  TextureManager::draw(this->numberTexture, srcRectNum , destRectNum);
  if (!isInFlag) {
    SDL_SetTextureAlphaMod(this->flagsTexture, 160);
  }else{
    SDL_SetTextureAlphaMod(this->flagsTexture, 255);
  }
  TextureManager::draw(this->flagsTexture, srcRectFlag , destRectFlag);
}

bool Tile::isInsideFlag(int x, int y){
  if (x > destRectFlag.x && x < (destRectFlag.w + destRectFlag.x) && y > destRectFlag.y && y < (destRectFlag.y + destRectFlag.h)) {
    isInFlag = true;
  }else{
    isInFlag = false;
  }
  return isInFlag;
}

gopo::Effect* Tile::getEffect(){
  return this->eff;
}

Tile::~Tile(){
  SDL_DestroyTexture(objTexture);
  SDL_DestroyTexture(numberTexture);
  SDL_DestroyTexture(flagsTexture);
}
