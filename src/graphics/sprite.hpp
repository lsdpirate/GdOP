#ifndef _SPRITE_H_
#define _SPRITE_H_

namespace graphics{
    class Sprite {

        public:
            Sprite(const char * assetLocation){};
            virtual void update() = 0;
            virtual void render() = 0;
            virtual bool isInside(int, int) = 0;
            virtual bool isInsideCircular(int, int) = 0;
        
    };
}

#endif