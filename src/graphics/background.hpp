#ifndef _H_BACKGROUND_
#define _H_BACKGROUND_
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>

namespace graphics{
  class Background{
  public:
    Background(const char* bg);
    void update(SDL_Rect);
    void render();
    ~Background();
  private:
    SDL_Texture* objTexture;
    SDL_Rect srcRectObj,destRectObj;
  };
}

#endif
