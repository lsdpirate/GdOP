#ifndef _H_BUTTON_
#define _H_BUTTON_
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "sprite.hpp"

namespace graphics{
  class Button : Sprite{
  public:
    Button(const char* filename,const char* text, SDL_Rect src, SDL_Rect dst);
    Button(const char* filename,const char* text, SDL_Rect src, SDL_Rect dst,SDL_Color textColor);
    void update();
    void update(std::string playerName);
    bool isInside(int x, int y);
    bool isInsideCircular(int x, int y);
    void render();
    void highlight(bool);
    ~Button();
  private:
    bool isIn;
    SDL_Color textColor;
    SDL_Texture* objTexture;
    SDL_Texture* textTexture;
    SDL_Rect srcRectObj,destRectObj;
    SDL_Rect srcRectText,destRectText;
  };
}

#endif
